import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { EdgeServiceService } from '../edge-service.service';
declare var $: any;

@Component({
  selector: 'app-visit',
  templateUrl: './visit.component.html',
  styleUrls: ['./visit.component.css']
})
export class VisitComponent implements OnInit {

  //spinners
  loading: boolean = false;
  delete: boolean = false;
  view: boolean = false;
  save: boolean = false;
  //page
  loadingPage: boolean = false;
  recordVisit: boolean = true;
  //message
  noVisitData: boolean = false;
  deleteMessage: any = '';

  mappedReadings: boolean = false;

  cancelMode: boolean = false;
  newVisit: boolean = true;
  viewMode: boolean = false;

  customers: any = [];
  customerDeet: any = []
  visit: any = [];
  storeUnits: any = [];

  rTypes: any = [];
  rItems: any = [];
  analysis: any = [];
  water: any = [];
  chemical: any = [];
  toBeDeletedReadings: any = [];

  aId: any = '';
  cId: any = '';
  wId: any = '';
  visitId: any = '';
  custName: any = '';


  constructor(private visitService: EdgeServiceService, private fb: FormBuilder) {

  }

  custForm = new FormGroup({
    custId: new FormControl(null, [Validators.required]),
  });

  get custId() { return this.custForm.get('custId'); }


  ngOnInit() {
    this.getCustomers();
  }

  readingsData: any = [];



  getCustomers() {
    this.loading = false;
    this.view = false;
    this.save = false;

    this.visitService.getCustomerDetails().subscribe((custData: any) => {
      if (custData != null && custData.length > 0) {
        custData.forEach((cust: any) => {
          this.customers.push({ id: cust.id, name: cust.contactPersonName });
        });

        this.getVisits();
      }
    })
  }

  getVisits() {
    this.visitService.getVisitDetails().subscribe((visitData: any) => {
      if (visitData != null && visitData.length > 0) {
        visitData.forEach((vis: any) => {
          this.customers.forEach((cData: any) => {
            if (vis.customerId == cData.id)
              this.visit.push({ id: vis.id, customerId: cData.id, customerName: cData.name, personId: vis.personId, startDate: vis.startDate, endDate: vis.endDate });
          });
        });
      }
      else
        this.noVisitData = true;

      this.loadingPage = true;
    });
  }

  selectCust() {
    this.loading = true;
    this.custName = '';
    this.newVisit = true;

    let startDate = formatDate(new Date(), 'dd-MM-yyyy hh:mm:ss a', 'en-US');
    //let inTime = formatDate(startDate, 'hh:mm:ss a', 'en-US');
    console.log(startDate);
    //console.log(inTime)
    this.visitService.recordVisit(1, this.custId.value, new Date(), new Date()).subscribe(visitData => {
      if (visitData != null) {
        this.visitId = visitData.id;
        this.getCustomerbyID(this.custId.value);
        this.getTypes();
      }
    })
  }

  getCustomerbyID(custId: any) {
    this.visitService.getCustomerbyId(custId).subscribe((custData: any) => {
      if (custData != null) {
        this.custName = custData.contactPersonName;
        //loop storage units for each customer
        this.storeUnits = [];
        if (custData.customerStorageUnitMappings != null && custData.customerStorageUnitMappings.length > 0)
          custData.customerStorageUnitMappings.forEach((unitData: any) => {
            this.storeUnits.push({ id: unitData.storageUnit.id, unitName: unitData.storageUnit.unitName });
          })
        else
          this.storeUnits = [];

        this.customerDeet.push({ id: custData.id, companyName: custData.companyName, contactPersonName: custData.contactPersonName, physicalAddress: custData.physicalAddress, contactPersonPhone: custData.contactPersonPhone, contactPersonEmail: custData.contactPersonEmail, ccEmailOne: custData.ccEmailOne, ccEmailTwo: custData.ccEmailTwo, storeUnits: this.storeUnits });
        console.log(this.customerDeet);
      }

      this.loading = false;
      this.view = false;
      this.recordVisit = false;
    });
  }

  getTypes() {
    this.rTypes = [];

    this.visitService.getReadingTypes().subscribe(data => {
      if (data != null && data.length > 0) {
        data.forEach((types: any) => {
          if (types.typeName.toUpperCase() == "ANALYSIS") this.aId = types.id;
          if (types.typeName.toUpperCase() == "WATER") this.wId = types.id;
          if (types.typeName.toUpperCase() == "CHEMICAL") this.cId = types.id;
          this.rTypes.push(types);
        });

        this.getItems();
      }
    })
  }

  getItems() {
    this.rItems = [];
    this.analysis = [];
    this.water = [];
    this.chemical = [];

    this.visitService.getReadingItems().subscribe(data => {
      if (data != null && data.length > 0) {
        data.forEach((items: any) => {
          this.rItems.push(items);
        });
        this.rItems.forEach((item: any) => {
          if (item.readingTypeId == this.aId) this.analysis.push(item);
          if (item.readingTypeId == this.wId) this.water.push(item);
          if (item.readingTypeId == this.cId) this.chemical.push(item);
        });

        if (!this.newVisit) this.getReadingsByVisitId(this.visitId);
        if (this.newVisit) this.editReadingsDisplay = false; else this.editReadingsDisplay = true;
      }
    });
  }

  getReadingsByVisitId(visitId: any) {
    this.editReadingsDisplay = true;
    this.readingsIdArray = [];

    this.visitService.getReadingsByVisitId(visitId).subscribe((readings: any) => {
      if (readings != null && readings.length > 0) {
        this.readingsData = [];

        readings.forEach((data: any) => {
          // (<HTMLInputElement>document.getElementById("text" + data.readingItemId + data.storageUnitId+"text")).innerHTML = data.amount;
          (<HTMLInputElement>document.getElementById("text" + data.readingItemId + data.storageUnitId)).value = data.amount;

          this.readingsData.push({ id: data.id, visitId: data.visitId, storageUnitId: data.storageUnitId, readingItemId: data.readingItemId, amount: data.amount })
        });
        //to get the id of the reading data while updating
        this.readingsIdArray = this.readingsData;
        console.log(this.readingsIdArray);
        this.newVisit = false;
      }
    })
  }
  readingsIdArray: any = [];
  // newReadingsData: any = [];

  saveReadings() {
    this.save = true;
    this.readingsData = [];
    //  this.newReadingsData = [];

    if (this.viewMode) {
      for (let i = 0; i < this.analysis.length; i++) {
        for (let j = 0; j < this.storeUnits.length; j++) {
          for (let k = 0; k < this.readingsIdArray.length; k++) {
            let checkId = (<HTMLInputElement>document.getElementById("text" + this.analysis[i].id + this.storeUnits[j].id)).value;
            //for updating reading data
            if (this.readingsIdArray[k].storageUnitId == this.storeUnits[j].id && this.readingsIdArray[k].readingItemId == this.analysis[i].id) {
              this.readingsData.push({ id: this.readingsIdArray[k].id, visitId: this.visitId, storageUnitId: this.storeUnits[j].id, readingItemId: this.analysis[i].id, amount: checkId })
            }
          }
        }
      }

      let eCount = 0;
      this.readingsData.forEach((updatedData: any) => {
        this.visitService.editReadings(updatedData).subscribe(data => {
          eCount++;
          if (eCount == this.readingsData.length) {            
            this.save = false;
            this.editReadingsDisplay = true;
          }
        })
      });
      
    }
    else {
      for (let i = 0; i < this.analysis.length; i++) {
        for (let j = 0; j < this.storeUnits.length; j++) {
          let checkId = (<HTMLInputElement>document.getElementById("text" + this.analysis[i].id + this.storeUnits[j].id)).value;
          if (checkId == '' || checkId.length == 0)
            this.readingsData.push({ visitId: this.visitId, storageUnitId: this.storeUnits[j].id, readingItemId: this.analysis[i].id, amount: null })
          else
            this.readingsData.push({ visitId: this.visitId, storageUnitId: this.storeUnits[j].id, readingItemId: this.analysis[i].id, amount: checkId })
        }
      }
      console.log(this.readingsData);

      let nCount = 0;
      this.readingsData.forEach((readings: any) => {
        this.visitService.createReadings(readings).subscribe(data => {
          nCount++;
          if (nCount == this.readingsData.length) {
            this.editReadingsDisplay = true;
            this.save = false;
          }
        })
      });
    }

  }

  editReadingsDisplay: boolean = true;

  editReadings() {
    this.editReadingsDisplay = false;
    console.log(this.readingsData);
    this.readingsData.forEach((data: any) => {
      (<HTMLInputElement>document.getElementById("text" + data.readingItemId + data.storageUnitId)).value = data.amount;
      console.log((<HTMLInputElement>document.getElementById("text" + data.readingItemId + data.storageUnitId)));
    });
  }

  viewVisit(visitId: any) {
    this.view = true;
    this.viewMode = true;

    this.visitId = visitId;
    this.visitService.getVisitDetailsByVisitId(visitId).subscribe((visitData: any) => {
      this.newVisit = false;
      this.getCustomerbyID(visitData.customerId);
      this.getTypes();
    })
  }


  deleteVisitConfirm(visitId: any) {
    this.visitId = visitId;
    this.deleteMessage = '';
    this.delete = false;
    this.mappedReadings = false;

    this.visitService.getVisitDetailsByVisitId(visitId).subscribe((visitData: any) => {
      if (visitData != null) {

        this.toBeDeletedReadings = [];
        if (visitData.readings != null && visitData.readings.length > 0) {
          this.mappedReadings = true;
          visitData.readings.forEach((readings: any) => {
            this.toBeDeletedReadings.push(readings.id);
          })
        }
      }
    });

  }

  deleteReadings() {
    this.delete = true;
    console.log(this.mappedReadings);
    console.log(this.toBeDeletedReadings);
    if (this.mappedReadings) {
      this.deleteMessage = "Readings are being erased..";
      let count = 0;
      this.toBeDeletedReadings.forEach((readingsId: any) => {
        this.visitService.deleteReadings(readingsId).subscribe(data => {
          console.log(data);
          if (data == null) count++;
          console.log(count);
          if (this.toBeDeletedReadings.length == count) this.deleteVisit();
        })
      })
    }
    else
      this.deleteVisit();
  }

  deleteVisit() {
    this.deleteMessage = "Visit is being deleted..";

    this.visitService.deleteVisit(this.visitId).subscribe(data => {
      if (data == null) {
        $('#staticBackdrop').modal('hide');
        this.visit = this.visit.filter((x: any) => x.id !== this.visitId)
        if (this.cancelMode) this.recordVisit = true;
      }
      this.cancelMode = false;
      this.custForm.reset();
    })
  }

  cancelVisit() {
    this.cancelMode = true;
    this.deleteVisitConfirm(this.visitId);
  }

}
