import { TestBed } from '@angular/core/testing';

import { EdgeServiceService } from './edge-service.service';

describe('EdgeServiceService', () => {
  let service: EdgeServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EdgeServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
