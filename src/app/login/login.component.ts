import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  loginForm = new FormGroup({
    userName: new FormControl('', [Validators.required]),
    userPassword: new FormControl('', [Validators.required]),
  });

  get userName() { return this.loginForm.get('userName'); }
  get userPassword() { return this.loginForm.get('userPassword'); }

  returnUrl: string;

  ngOnInit() {


  }

  onSubmit() {
    console.log(this.userName);
    //this.router.navigate(['home']);
  }
}
