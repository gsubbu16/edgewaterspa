import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './users/users.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Interceptor } from 'src/Utilities/Interceptor';
import { CustomersComponent } from './customers/customers.component';
import { VisitComponent } from './visit/visit.component';
import { StorageUnitsComponent } from './storage-units/storage-units.component';
import { ConsumptionComponent } from './consumption/consumption.component';
import { Charts1Component } from './charts1/charts1.component';
import { ReportPdfComponent } from './report-pdf/report-pdf.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    UsersComponent,
    CustomersComponent,
    VisitComponent,
    StorageUnitsComponent,
    ConsumptionComponent,
    Charts1Component,
    ReportPdfComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    FormsModule,   
    ReactiveFormsModule
  ],
  providers: [ {
    provide: HTTP_INTERCEPTORS,
    useClass: Interceptor,
    multi: true
  }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
