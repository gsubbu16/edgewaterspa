import { Component } from '@angular/core';
//import  {CanvasJS} from '@angular/animations';
import jspdf from 'jspdf';
import html2canvas from 'html2canvas';
@Component({
  selector: 'app-root',
  templateUrl: './report-pdf.component.html',
  
  styleUrls: ['./report-pdf.component.css']
})
export class ReportPdfComponent {
  title = 'app-edge2';
  name:String='PROCESS WATER SERVICE REPORT ';
  clickMessage1:String='Details';
  clickMessage2:String='Storage Unit type';
  clickMessage3:String='Results';

  
  onClickMe1(){
    this.clickMessage1='Details of Customers';
   
  }
  onClickMe2(){
    this.clickMessage2='Storage Units';
   
  }
  onClickMe3(){
    this.clickMessage3='Operation results';
    
   
  }

  downloadPDF1(){
    
      var element=document.getElementById('t3')

      html2canvas(element).then((canvas)=>{
        console.log(canvas)
        var pgHeight=500
        var imgWidth=200;
        var imgHeight=canvas.height*imgWidth/canvas.width;
        var imageData1=canvas.toDataURL('image/png')
        
        var doc1=new jspdf;
        var img1=new Image();
        img1.src='http://edgecompletewater.com.au/wp-content/uploads/2016/09/edge-complete-ws-logo.jpg';
        //doc1.addImage(image1,0,0,40,80);
        doc1.addImage(imageData1,0,10,imgWidth,imgHeight);
        doc1.save("image.pdf");
        //doc1.loadFile(imageData1);
        
        var imgData = 'http://edgecompletewater.com.au/wp-content/uploads/2016/09/edge-complete-ws-logo.jpg';

            function genratePDF() {
                getDataUri(imgData, function (dataUri) {                    
                    generar(dataUri);
                });
            }

            function getDataUri(url, cb)
            {             
                var image = new Image();

                image.onload = function () {
                    var canvas = document.createElement('canvas');
                    //canvas.width = this.width;
                    //canvas.height = this.naturalHeight;

                    //next three lines for white background in case png has a transparent background
                    var ctx = canvas.getContext('2d');
                    ctx.fillStyle = '#fff';  /// set white fill style
                    ctx.fillRect(0, 0, canvas.width, canvas.height);

                    canvas.getContext('2d').drawImage(url, 0, 0);

                    cb(canvas.toDataURL('image/jpeg'));
                };
                image.src = url;                
            }

            function generar(img) {                
              //  var texto = input.value;
                var pdf = new jspdf();                
                pdf.addImage(img, 'JPG', 15, 40, 100, 100);
              //  pdf.text(30, 30, texto);
                pdf.save('my_pdf.pdf');                
            }
      })
   
  }

}
