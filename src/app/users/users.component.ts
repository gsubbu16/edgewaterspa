import { Component, OnInit } from '@angular/core';
import { EdgeServiceService } from '../edge-service.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: any = [];
  userRoleData: any = ["Admin", "Reporter"];

  userId: any = '';

  loadingPage: boolean = false;
  addUserDisplay: boolean = false;
  loading: boolean = false;
  editMode: boolean = false;
  showAddUser: boolean = false;
  delete: boolean = false;
  noUserData: boolean = false;

  constructor(private userService: EdgeServiceService) { }

  registerForm = new FormGroup({
    userFullName: new FormControl('', [Validators.required]),
    userPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
    userRole: new FormControl('', [Validators.required]),
    userEmail: new FormControl('', [Validators.required, Validators.email]),
  });

  get userFullName() { return this.registerForm.get('userFullName'); }
  get userPassword() { return this.registerForm.get('userPassword'); }
  get userRole() { return this.registerForm.get('userRole'); }
  get userEmail() { return this.registerForm.get('userEmail'); }



  ngOnInit() {
    this.onLoad();
  }

  onLoad() {
    this.addUserDisplay = false;
    this.loading = false;
    this.editMode = false;
    this.noUserData = false;
    this.showAddUser = false;

    this.userService.getUserDetails().subscribe(data => {
      if (data != null && data.length > 0) {
        data.forEach((x: any) => {
          this.users.push(x);
        });
      }
      else
        this.noUserData = true;

      this.loadingPage = true;
      //console.log(this.users);
    });
  }

  addUser() {
    this.addUserDisplay = true;
    this.showAddUser = true;

    this.registerForm.reset();
  }

  editUsers(user: any) {
    this.addUserDisplay = true;
    this.showAddUser = true;
    this.editMode = true;

    this.userId = user.id;
    this.registerForm.setValue({
      userFullName: user.fullName,
      userPassword: user.password,
      userRole: user.role,
      userEmail: user.email
    });
  }

  deleteUserConfirm(userId: any) {
    this.userId = userId;
  }

  deleteUser() {
    this.delete = true;

    this.userService.deleteUserDetails(this.userId).subscribe(data => {
      if (data == null) {
        $('#staticBackdrop').modal('hide');
        this.users = this.users.filter((x: any) => x.id !== this.userId)
        this.delete = false;
      }
    });
  }

  registerUser() {    
    this.loading = true;
    this.users = [];

    if (this.editMode) {
      this.userService.editUserDetails(this.registerForm.value, this.userId).subscribe(data => {
        this.loadingPage = false;
        //keep load function here to skip the service wait time with spinner bootstrap
        this.onLoad();
      })
    }
    else {
      console.log(this.userEmail);
      this.userService.registerUserDetails(this.registerForm.value).subscribe(data => {
        this.loadingPage = false;
        //keep load function here to skip the service wait time with spinner bootstrap
        this.onLoad();
      });
    }
  }

  cancelUser() {
    this.addUserDisplay = false;
    this.showAddUser = false;

    //this.registerForm.reset();
  }

}
