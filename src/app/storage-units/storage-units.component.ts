import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EdgeServiceService } from '../edge-service.service';
declare var $: any;

@Component({
  selector: 'app-storage-units',
  templateUrl: './storage-units.component.html',
  styleUrls: ['./storage-units.component.css']
})
export class StorageUnitsComponent implements OnInit {

  //button
  showAddUnit: boolean = false;

  //page
  unitDisplay: boolean = true;
  loadingPage: boolean = false;

  //message
  noUnitData: boolean = false;

  //spinners
  loading: boolean = false;
  delete: boolean = false;

  units: any = [];
  unitId: any = ''
  deleteMessage: any = '';
  editMode: boolean = false;
  capacityPattern:any = "^[0-9]{1,10}(\.[0-9]{1,2})?$";

  constructor(private unitService: EdgeServiceService) { }

  unitForm = new FormGroup({
    unitName: new FormControl('', [Validators.required]),
    typeName: new FormControl('', [Validators.required]),
    capacity: new FormControl('', [Validators.required,Validators.pattern(this.capacityPattern)])
  });

  get unitName() { return this.unitForm.get('unitName'); }
  get typeName() { return this.unitForm.get('typeName'); }
  get capacity() { return this.unitForm.get('capacity'); }

  ngOnInit() {
    this.onLoad();
  }

  onLoad() {
    this.showAddUnit = false;
    this.unitDisplay = true;
    this.loading = false;
    this.noUnitData = false;


    this.unitService.getStorageUnitDetails().subscribe(data => {
      if (data != null && data.length > 0) {
        data.forEach((unitData: any) => {
          this.units.push(unitData);
        });
      }
      else
        this.noUnitData = true;

      this.loadingPage = true;
    })
  }

  addStorageUnit() {
    this.showAddUnit = true;
    this.unitDisplay = false;

    this.unitForm.reset();
  }

  editUnitDetails(unit: any) {
    this.editMode = true;
    this.showAddUnit = true;
    this.unitDisplay = false;

    this.unitId = unit.id;
    this.unitForm.setValue({
      unitName: unit.unitName,
      typeName: unit.typeName,
      capacity: unit.capacity
    })
  }

  deleteUnitConfirm(unit: any) {
    this.unitId = unit.id;
    this.deleteMessage = '';
  }

  deleteUnit() {
    this.deleteMessage = "Storage unit is being deleted.."
    this.delete = true;

    this.unitService.deleteUnitDetails(this.unitId).subscribe(data => {
      if (data == null) {
        $('#staticBackdrop').modal('hide');
        this.units = this.units.filter((x: any) => x.id !== this.unitId)
        this.delete = false;
      }
    })
  }

  addUnit() {
    this.loading = true;
    this.units = [];

    if (this.editMode) {
      this.unitService.editUnitDetails(this.unitForm.value, this.unitId).subscribe(data => {
        this.loadingPage = false;
        //keep load function here to skip the service wait time with spinner bootstrap
        this.onLoad();
      })
    }
    else {
      this.unitService.addStorageUnit(this.unitForm.value).subscribe(data => {
        this.loadingPage = false;
        //keep load function here to skip the service wait time with spinner bootstrap
        this.onLoad();
      })
    }
  }

  cancelUnit() {
    this.showAddUnit = false;
    this.unitDisplay = true;

  }

}
