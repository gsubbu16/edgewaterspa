import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const url = "http://13.236.220.88:3000/v1/";

@Injectable({
  providedIn: 'root'
})

export class EdgeServiceService {

  userBaseUrl = url + 'users';
  custBaseUrl = url + 'customers';
  storeBaseUrl = url + 'storageUnits';
  storeUnitMapBaseUrl = url + 'customerStorageUnitMappings';
  readingTypeUrl = url + 'readingTypes';
  readingItemUrl = url + 'readingItems';
  visitUrl = url + 'visits';
  readingsUrl = url + 'readings';

  constructor(private http: HttpClient) { }

  //Visit Service
  getVisitDetails() {
    return this.http.get(this.visitUrl, httpOptions).pipe(
      catchError(this.handleError<any>('visit details cannot be fetched!'))
    );
  }

  getVisitDetailsByVisitId(visitId: any) {
    return this.http.get(this.visitUrl + '/' + visitId, httpOptions).pipe(
      catchError(this.handleError<any>('visit details cannot be fetched!'))
    );
  }

  recordVisit(pId: any, cId: any, sDate: any, eDate: any): Observable<any> {
    var jsonObject = {};
    jsonObject["personId"] = pId;
    jsonObject["customerId"] = cId;
    jsonObject["startDate"] = sDate;
    jsonObject["endDate"] = eDate;
    return this.http.post<any>(this.visitUrl, jsonObject, httpOptions).pipe(
      catchError(this.handleError<any>('visit cannot be created!'))
    );
  }

  deleteVisit(visitId: any): Observable<any> {
    return this.http.delete(this.visitUrl + '/' + visitId, httpOptions).pipe(
      catchError(this.handleError<any>('failed to delete the visit!'))
    );
  }

  //Readings Service

  createReadings(readings: any): Observable<any> {
    console.log(readings);
    var jsonObject = {};
    jsonObject["visitId"] = readings.visitId;
    jsonObject["storageUnitId"] = readings.storageUnitId;
    jsonObject["readingItemId"] = readings.readingItemId;
    jsonObject["amount"] = readings.amount;
    return this.http.post<any>(this.readingsUrl, jsonObject, httpOptions).pipe(
      catchError(this.handleError<any>('new readings cannot be saved!'))
    );
  }

  editReadings(readings: any): Observable<any> {
    console.log(readings);
    var jsonObject = {};
    jsonObject["id"]=readings.id;
    jsonObject["visitId"] = readings.visitId;
    jsonObject["storageUnitId"] = readings.storageUnitId;
    jsonObject["readingItemId"] = readings.readingItemId;
    jsonObject["amount"] = readings.amount;
    return this.http.put<any>(this.readingsUrl, jsonObject, httpOptions).pipe(
      catchError(this.handleError<any>('readings cannot be updated!'))
    );
  }

  getReadingsByVisitId(visitId: any): Observable<any> {
    return this.http.get(this.readingsUrl + '/visitId/' + visitId, httpOptions).pipe(
      catchError(this.handleError<any>('visit details cannot be fetched!'))
    );
  }

  deleteReadings(readingId: any): Observable<any> {
    return this.http.delete(this.readingsUrl + '/' + readingId, httpOptions).pipe(
      catchError(this.handleError<any>('failed to delete the readings!'))
    );
  }


  //Customer Service

  getCustomerDetails() {
    return this.http.get(this.custBaseUrl, httpOptions).pipe(
      catchError(this.handleError<any>('fetching customer details failed!'))
    );
  }

  getCustomerbyId(custId: any) {
    return this.http.get(this.custBaseUrl + '/' + custId, httpOptions).pipe(
      catchError(this.handleError<any>('customer details cannot be fetched!'))
    );
  }

  registerCustomerDetails(newCustData: any): Observable<any> {
    var jsonObject = {};
    jsonObject["companyName"] = newCustData.companyName;
    jsonObject["physicalAddress"] = newCustData.customerAddress;
    jsonObject["contactPersonName"] = newCustData.customerName;
    jsonObject["contactPersonPhone"] = newCustData.customerPhone;
    jsonObject["contactPersonEmail"] = newCustData.customerEmail;
    jsonObject["ccEmailOne"] = newCustData.customerEmailOne;
    jsonObject["ccEmailTwo"] = newCustData.customerEmailTwo;

    return this.http.post<any>(this.custBaseUrl, jsonObject, httpOptions).pipe(
      catchError(this.handleError<any>('new customer cannot be registered!'))
    );
  }

  editCustomerDetails(custData: any, custId: any): Observable<any> {
    var jsonObject = {};
    jsonObject["id"] = custId;
    jsonObject["companyName"] = custData.companyName;
    jsonObject["physicalAddress"] = custData.customerAddress;
    jsonObject["contactPersonName"] = custData.customerName;
    jsonObject["contactPersonPhone"] = custData.customerPhone;
    jsonObject["contactPersonEmail"] = custData.customerEmail;
    jsonObject["ccEmailOne"] = custData.customerEmailOne;
    jsonObject["ccEmailTwo"] = custData.customerEmailTwo;

    return this.http.put<any>(this.custBaseUrl, jsonObject, httpOptions).pipe(
      catchError(this.handleError<any>('customer details cannot be edited!'))
    );
  }

  deleteCustDetails(custId: any): Observable<any> {
    return this.http.delete(this.custBaseUrl + '/' + custId, httpOptions).pipe(
      catchError(this.handleError<any>('failed to delete the user!'))
    );
  }

  //Storage Unit mapping

  mapUnitToCust(custId: any, unitIds: any): Observable<any> {
    var jsonObject = {};
    jsonObject["customerId"] = custId;
    jsonObject["storageUnitIds"] = unitIds;
    return this.http.post<any>(this.storeUnitMapBaseUrl, jsonObject, httpOptions).pipe(
      catchError(this.handleError<any>('Storage unit cannot be mapped to the customer!'))
    );
  }

  updateUnitMappings(custId: any, unitIds: any): Observable<any> {
    var jsonObject = {};
    jsonObject["customerId"] = custId;
    jsonObject["storageUnitIds"] = unitIds;
    return this.http.put<any>(this.storeUnitMapBaseUrl, jsonObject, httpOptions).pipe(
      catchError(this.handleError<any>('Storage unit mappings cannot be updated!'))
    );
  }

  deleteUnitMapping(custId: any): Observable<any> {
    return this.http.delete(this.storeUnitMapBaseUrl + '/customerId/' + custId, httpOptions).pipe(
      catchError(this.handleError<any>('failed to delete the mapping!'))
    );
  }

  //Storage Unit Service

  getStorageUnitDetails() {
    return this.http.get(this.storeBaseUrl, httpOptions).pipe(
      catchError(this.handleError<any>('fetching storage unit details failed!'))
    );
  }

  addStorageUnit(unitData: any): Observable<any> {
    var jsonObject = {};
    jsonObject["unitName"] = unitData.unitName;
    jsonObject["typeName"] = unitData.typeName;
    jsonObject["capacity"] = unitData.capacity;

    return this.http.post<any>(this.storeBaseUrl, jsonObject, httpOptions).pipe(
      catchError(this.handleError<any>('new unit cannot be created!'))
    );
  }

  editUnitDetails(editedUnitData: any, unitId: any): Observable<any> {
    var jsonObject = {};
    jsonObject["id"] = unitId;
    jsonObject["unitName"] = editedUnitData.unitName;
    jsonObject["typeName"] = editedUnitData.typeName;
    jsonObject["capacity"] = editedUnitData.capacity;

    return this.http.put<any>(this.storeBaseUrl, jsonObject, httpOptions).pipe(
      catchError(this.handleError<any>('unit details cannot be updated!'))
    );
  }

  deleteUnitDetails(unitId: any): Observable<any> {
    return this.http.delete(this.storeBaseUrl + '/' + unitId, httpOptions).pipe(
      catchError(this.handleError<any>('failed to delete the unit!'))
    );
  }

  // Reading Type Service

  getReadingTypes() {
    return this.http.get(this.readingTypeUrl, httpOptions).pipe(
      catchError(this.handleError<any>('reading types details cannot be fetched!'))
    );
  }

  //Reading Item Service

  getReadingItems() {
    return this.http.get(this.readingItemUrl, httpOptions).pipe(
      catchError(this.handleError<any>('reading items details cannot be fetched!'))
    );
  }

  addReadingItem(item: any, rTypeId: any): Observable<any> {
    var jsonObject = {};
    jsonObject["readingTypeId"] = rTypeId;
    jsonObject["itemName"] = item.itemName;
    jsonObject["operator"] = item.range.sign;
    jsonObject["rangeMin"] = item.range.minRange;
    jsonObject["rangeMax"] = item.range.maxRange;        
    jsonObject["chemicalName"] = item.chemicalName;

    return this.http.post<any>(this.readingItemUrl, jsonObject, httpOptions).pipe(
      catchError(this.handleError<any>('new item cannot be created!'))
    );
  }

  editReadingItem(item: any, itemId: any, rTypeId: any): Observable<any> {
    console.log(item);
    var jsonObject = {};
    jsonObject["id"] = itemId;
    jsonObject["readingTypeId"] = rTypeId;
    jsonObject["itemName"] = item.itemName;
    jsonObject["operator"] = item.range.sign;
    // jsonObject["rangeMin"] = item.range.minRange;
    // jsonObject["rangeMax"] = item.range.maxRange; 
    if (item.range.minRange!= null && item.range.minRange.length < 1) jsonObject["rangeMin"] = null; else jsonObject["rangeMin"] = item.range.minRange;
    if (item.range.maxRange!= null && item.range.maxRange.length < 1) jsonObject["rangeMax"] = null; else jsonObject["rangeMax"] = item.range.maxRange;
    jsonObject["chemicalName"] = item.chemicalName;

    return this.http.put<any>(this.readingItemUrl, jsonObject, httpOptions).pipe(
      catchError(this.handleError<any>('new item cannot be created!'))
    );
  }

  deleteReadingItem(itemId: any): Observable<any> {
    return this.http.delete(this.readingItemUrl + '/' + itemId, httpOptions).pipe(
      catchError(this.handleError<any>('failed to delete the item!'))
    );
  }

  // User Service

  getUserDetails() {
    return this.http.get(this.userBaseUrl, httpOptions).pipe(
      catchError(this.handleError<any>('fetching user details failed!'))
    );
  }

  registerUserDetails(newUserData: any): Observable<any> {
    var jsonObject = {};
    jsonObject["email"] = newUserData.userEmail;
    jsonObject["fullName"] = newUserData.userFullName;
    jsonObject["role"] = newUserData.userRole;
    jsonObject["password"] = newUserData.userPassword;

    return this.http.post<any>(this.userBaseUrl, jsonObject, httpOptions).pipe(
      catchError(this.handleError<any>('new user cannot be registered!'))
    );
  }

  editUserDetails(editedUserData: any, userId: any): Observable<any> {
    var jsonObject = {};
    jsonObject["id"] = userId;
    jsonObject["email"] = editedUserData.userEmail;
    jsonObject["fullName"] = editedUserData.userFullName;
    jsonObject["role"] = editedUserData.userRole;
    jsonObject["password"] = editedUserData.userPassword;

    return this.http.put<any>(this.userBaseUrl, jsonObject, httpOptions).pipe(
      catchError(this.handleError<any>('user details cannot be changed!'))
    );
  }

  deleteUserDetails(userId: any): Observable<any> {
    return this.http.delete(this.userBaseUrl + '/' + userId, httpOptions).pipe(
      catchError(this.handleError<any>('failed to delete the user!'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
