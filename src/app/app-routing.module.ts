import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsumptionComponent } from './consumption/consumption.component';
import { CustomersComponent } from './customers/customers.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { StorageUnitsComponent } from './storage-units/storage-units.component';
import { UsersComponent } from './users/users.component';
import { VisitComponent } from './visit/visit.component';
import { Charts1Component } from './charts1/charts1.component';
import { ReportPdfComponent } from './report-pdf/report-pdf.component';
//import { ReportPdfComponent } from './report-pdf/report-pdf.component';
const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'users', component: UsersComponent },
  { path: 'customers', component: CustomersComponent },
  { path: 'storage', component: StorageUnitsComponent },
  { path: 'consumption', component: ConsumptionComponent },
  { path: 'visit', component: VisitComponent },
  { path: 'chart', component: Charts1Component },
  { path: 'report', component: ReportPdfComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
