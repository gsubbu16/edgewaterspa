import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { EdgeServiceService } from '../edge-service.service';
declare var $: any;

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {


  constructor(private custService: EdgeServiceService, private formBuilder: FormBuilder) {
    this.mapForm = this.formBuilder.group({
      mapData: new FormArray([], this.minSelectedCheckboxes(1))
    });
  }
  //button
  showAddCustomer: boolean = false;
  //page
  addCustDisplay: boolean = false;
  loadingPage: boolean = false;
  custDisplay: boolean = true;
  mapCustomerSU: boolean = false;
  //spinners
  loading: boolean = false;
  delete: boolean = false;
  register: boolean = false;

  customers: { id: any, companyName: any, contactPersonName: any, physicalAddress: any, contactPersonPhone: any, contactPersonEmail: any, ccEmailOne: any, ccEmailTwo: any, storeUnits: [] }[] = [];
  displayNewCustData: any = [];
  storageUnits: any = [];
  mappedUnits: any = [];
  phonePattern: any = "^[0-9]{10}$";

  custId: any = '';
  deleteMessage: any = '';
  noCustData: boolean = false;
  unitsMapped: boolean = false;
  editMode: boolean = false;

  customerForm = new FormGroup({
    companyName: new FormControl('', [Validators.required]),
    customerName: new FormControl('', [Validators.required]),
    customerAddress: new FormControl('', [Validators.required]),
    customerPhone: new FormControl('', [Validators.required, Validators.pattern(this.phonePattern)]),
    customerEmail: new FormControl('', [Validators.required, Validators.email]),
    customerEmailOne: new FormControl('', []),
    customerEmailTwo: new FormControl('', [])
  });

  get companyName() { return this.customerForm.get('companyName'); }
  get customerName() { return this.customerForm.get('customerName'); }
  get customerAddress() { return this.customerForm.get('customerAddress'); }
  get customerPhone() { return this.customerForm.get('customerPhone'); }
  get customerEmail() { return this.customerForm.get('customerEmail'); }
  get customerEmailOne() { return this.customerForm.get('customerEmailOne'); }
  get customerEmailTwo() { return this.customerForm.get('customerEmailTwo'); }

  mapForm: FormGroup;

  get mapFormArray() {
    return this.mapForm.controls.mapData as FormArray;
  }

  ngOnInit() {
    this.onLoad();   
  }

  onLoad() {
    this.loading = false;
    this.register = false;
    this.addCustDisplay = false;
    this.showAddCustomer = false;
    this.loadingPage = false;
    this.editMode = false;
    this.customers = [];

    console.log("onload")
    this.getCustomer();    
  }

  getCustomer(){
    this.custService.getCustomerDetails().subscribe(data => {
      if (data != null && data.length > 0) {
        data.forEach((custData: any) => {

          //loop storage units for each customer
          let storeUnits: any = [];
          if (custData.customerStorageUnitMappings != null && custData.customerStorageUnitMappings.length > 0)
            custData.customerStorageUnitMappings.forEach((unitData: any) => {
              storeUnits.push({ id: unitData.storageUnit.id, unitName: unitData.storageUnit.unitName });
            })
          else
            storeUnits = [];

          this.customers.push({ id: custData.id, companyName: custData.companyName, contactPersonName: custData.contactPersonName, physicalAddress: custData.physicalAddress, contactPersonPhone: custData.contactPersonPhone, contactPersonEmail: custData.contactPersonEmail, ccEmailOne: custData.ccEmailOne, ccEmailTwo: custData.ccEmailTwo, storeUnits: storeUnits });
        });
        //sort the customers by ID after pushing
        this.customers = this.customers.sort((x, y) => x.id - y.id);
      }
      else
        this.noCustData = true;

      this.loadingPage = true;
    })
  }

  addCustomer() {
    this.showAddCustomer = true;
    this.custDisplay = false;
    this.addCustDisplay = true;

    this.customerForm.reset();
    this.mapForm.reset();
  }

  editCustDetails(custData: any) {
    console.log(custData);
    this.custDisplay = false;
    this.addCustDisplay = true;
    this.custId = custData.id;
    this.editMode = true;
    this.showAddCustomer = true;
    custData.storeUnits.forEach((units:any) => {
      this.mappedUnits.push(units.id)
    });

    this.customerForm.setValue({
      companyName: custData.companyName,
      customerName: custData.contactPersonName,
      customerAddress: custData.physicalAddress,
      customerPhone: custData.contactPersonPhone,
      customerEmail: custData.contactPersonEmail,
      customerEmailOne: custData.ccEmailOne,
      customerEmailTwo: custData.ccEmailTwo
    })
  }

  registerCustomer() {       

    this.loading = true;

    this.displayNewCustData = [];
    this.storageUnits = [];

    this.resetmapFormArray();

    this.custService.getStorageUnitDetails().subscribe(data => {
      this.storageUnits = data;
      this.addCheckboxes();
      //console.log(data);
      if (this.editMode) {
        this.custService.editCustomerDetails(this.customerForm.value, this.custId).subscribe(data => {
          this.displayNewCustData.push(this.customerForm.value);
          // console.log(data);
          // console.log(this.mappedUnits);
          //check the mapped units
          for (let i = 0; i < this.storageUnits.length; i++) {
            for (let j = 0; j < this.mappedUnits.length; j++) {
              if (this.mappedUnits[j] == this.storageUnits[i].id)
                this.mapFormArray.setControl(i, new FormControl(true));
            }
          }
          this.addCustDisplay = false;
          this.mapCustomerSU = true;
          console.log(this.mapFormArray.controls)
        })
      }
      else {
        this.custService.registerCustomerDetails(this.customerForm.value).subscribe(data => {
          this.displayNewCustData.push(this.customerForm.value);
          this.custId = data.id;
          this.addCustDisplay = false;
          this.mapCustomerSU = true;
        })
      }
      console.log(this.displayNewCustData);

    })
  }

  mapStorageUnit() {
    this.register = true;
    //get the mapped units
    const selectedOrderIds = this.mapForm.value.mapData
      .map((checked, i) => checked ? this.storageUnits[i].id : null)
      .filter(v => v !== null)
    console.log(selectedOrderIds)

    if (this.editMode) {
      this.custService.updateUnitMappings(this.custId, selectedOrderIds).subscribe(data => {
        this.mapCustomerSU = false;
        this.custDisplay = true;
        this.onLoad();
      })
    } else {
      this.custService.mapUnitToCust(this.custId, selectedOrderIds).subscribe(data => {
        this.mapCustomerSU = false;
        this.custDisplay = true;
        this.onLoad();
      })
    }

  }

  deleteCustConfirm(custData: any) {
    this.custId = custData.id;
    this.unitsMapped = false;
    this.deleteMessage = '';
    console.log(custData);
    //check for store unit mappings
    if (custData.storeUnits.length > 0) this.unitsMapped = true;
  }

  deleteCustomer() {
    this.delete = true;

    //If yes, delete the mapping before deleting the customer
    if (this.unitsMapped) {
      this.deleteMessage = "Storage Unit is being unmapped.."
      this.custService.deleteUnitMapping(this.custId).subscribe(data => {
        this.deleteCustDetails();
      })
    }
    else
      this.deleteCustDetails();

  }

  deleteCustDetails() {
    this.deleteMessage = "Customer is being removed.."
    this.custService.deleteCustDetails(this.custId).subscribe(data => {
      if (data == null) {
        console.log(data)
        $('#staticBackdrop').modal('hide');
        this.customers = this.customers.filter((x: any) => x.id !== this.custId)
        this.delete = false;
      }
    });
  }

  cancelCustomer() {
    this.showAddCustomer = false;
    this.addCustDisplay = false;
    this.custDisplay = true;

    //this.customerForm.reset();
  }

  private addCheckboxes() {
    this.storageUnits.forEach(() => this.mapFormArray.push(new FormControl(false)));
  }

  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (mapFormArray: FormArray) => {
      const totalSelected = mapFormArray.controls
        // get a list of checkbox values (boolean)
        .map(control => control.value)
        // total up the number of checked checkboxes
        .reduce((prev, next) => next ? prev + next : prev, 0);

      // if the total is not greater than the minimum, return the error message
      return totalSelected >= min ? null : { required: true };
    };

    return validator;
  }

  resetmapFormArray() {
    const control = <FormArray>this.mapForm.controls['mapData'];
    console.log(control);
    for (let i = control.length - 1; i >= 0; i--) {
      control.removeAt(i)
    }
  }

}
