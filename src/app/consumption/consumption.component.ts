import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { timeout } from 'rxjs/operators';
import { EdgeServiceService } from '../edge-service.service';
declare var $: any;

@Component({
  selector: 'app-consumption',
  templateUrl: './consumption.component.html',
  styleUrls: ['./consumption.component.css']
})
export class ConsumptionComponent implements OnInit {

  //page
  loadingPage: boolean = false;
  analysisDisplay: boolean = false;
  waterDisplay: boolean = false;
  chemDisplay: boolean = false;
  //message
  deleteMessage: any = '';
  //spinners
  loading: boolean = false;
  delete: boolean = false;

  noItemData: boolean = false;
  editMode: boolean = false;

  aId: any = '';
  wId: any = '';
  cId: any = '';
  itemId: any = '';
  activeKey: any = '';

  rangePattern: any = "^-?[0-9]{1,4}(\.[0-9]{1,2})?$";

  rTypes: any = [];
  rItems: any = [];

  analysis: any = [];
  water: any = [];
  chemical: any = [];
  signData: any = [">", "<"];

  constructor(private consumService: EdgeServiceService) {

  }

  itemForm: FormGroup;

  checkValue: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    const sign = control.get('sign');
    const minRange = control.get('minRange');
    const maxRange = control.get('maxRange');

    if ((minRange.value !== null && maxRange.value !== null && minRange.value.length > 0 && maxRange.value.length > 0) && (sign.value === null || sign.value.length == 0)) {
      if (parseFloat(minRange.value) < parseFloat(maxRange.value)) return null;
      else return { minMax: true }

    } else if ((minRange.value !== null && sign.value !== null && minRange.value.length > 0 && sign.value.length > 0) && (maxRange.value === null || maxRange.value.length == 0)) {
      if (sign.value === ">") return null
      else return { signMin: true }

    } else if ((maxRange.value !== null && sign.value !== null && maxRange.value.length > 0 && sign.value.length > 0) && (minRange.value === null || minRange.value.length == 0)) {
      if (sign.value === "<") return null
      else return { signMax: true }
    }
    else
      return { yes: true };

  };

  get itemName() { return this.itemForm.get('itemName'); }
  get range() { return this.itemForm.get('range'); }
  get sign() { return this.itemForm.get('range.sign'); }
  get minRange() { return this.itemForm.get('range.minRange'); }
  get maxRange() { return this.itemForm.get('range.maxRange'); }
  get chemicalName() { return this.itemForm.get('chemicalName'); }

  ngOnInit() {
    this.itemForm = new FormGroup({
      itemName: new FormControl('', [Validators.required]),
      range: new FormGroup({
        sign: new FormControl(null, []),
        minRange: new FormControl(null, [Validators.pattern(this.rangePattern)]),
        maxRange: new FormControl(null, [Validators.pattern(this.rangePattern)])
      }, { validators: this.checkValue.bind(this) }),
      chemicalName: new FormControl('', [Validators.required])
    });

    this.onLoad();
  }

  onLoad() {
    this.activeKey = '';
    this.loadingPage = false;
    this.noItemData = false;

    this.analysisDisplay = true;
    this.waterDisplay = true;
    this.chemDisplay = true;

    this.getTypes();
  }

  getTypes() {
    this.loading = false;
    this.editMode = false;

    this.consumService.getReadingTypes().subscribe(data => {
      if (data != null && data.length > 0) {
        data.forEach((types: any) => {
          if (types.typeName.toUpperCase() == "ANALYSIS") this.aId = types.id;
          if (types.typeName.toUpperCase() == "WATER") this.wId = types.id;
          if (types.typeName.toUpperCase() == "CHEMICAL") this.cId = types.id;
          this.rTypes.push(types);
        });

        this.getItems();
      }
    })
  }

  getItems() {
    this.rItems = [];
    this.analysis = [];
    this.water = [];
    this.chemical = [];

    this.consumService.getReadingItems().subscribe(data => {
      if (data != null && data.length > 0) {
        data.forEach((items: any) => {
          this.rItems.push(items);
        });
        this.rItems.forEach((item: any) => {
          if (item.readingTypeId == this.aId) this.analysis.push(item);
          if (item.readingTypeId == this.wId) this.water.push(item);
          if (item.readingTypeId == this.cId) this.chemical.push(item);
        });

      }
      else
        this.noItemData = true;

      console.log(this.analysis)
      this.loadingPage = true;

      if (this.activeKey == 'A') {
        $(function () {
          $('#water').removeClass('show active')
          $('#chemical').removeClass('show active')
          $('#analysis').addClass('show active')
          $('#water-tab').removeClass('disabled');
          $('#chemical-tab').removeClass('disabled');
        })
      }
      if (this.activeKey == 'W') {
        $(function () {
          $('#analysis').removeClass('show active')
          $('#chemical').removeClass('show active')
          $('#water').addClass('show active')
          $('#analysis-tab').removeClass('disabled');
          $('#chemical-tab').removeClass('disabled');
        })
      }
      if (this.activeKey == 'C') {
        $(function () {
          $('#analysis').removeClass('show active')
          $('#water').removeClass('show active')
          $('#chemical').addClass('show active')
          $('#analysis-tab').removeClass('disabled');
          $('#water-tab').removeClass('disabled');
        })
      }

    })
  }

  addItem(key: any) {
    if (key == 'A') {
      this.analysisDisplay = false;
      $('#water-tab').addClass('disabled');
      $('#chemical-tab').addClass('disabled');
    }
    if (key == 'W') {
      this.waterDisplay = false;
      $('#analysis-tab').addClass('disabled');
      $('#chemical-tab').addClass('disabled');
    }
    if (key == 'C') {
      this.chemDisplay = false;
      $('#analysis-tab').addClass('disabled');
      $('#water-tab').addClass('disabled');
    }

    this.itemForm.reset();
  }

  editItemDetails(item: any, key: any) {
    console.log(item);
    this.editMode = true;
    this.itemId = item.id;

    if (key == 'A') {
      this.analysisDisplay = false;
      $('#water-tab').addClass('disabled');
      $('#chemical-tab').addClass('disabled');
    }
    if (key == 'W') {
      this.waterDisplay = false;
      $('#analysis-tab').addClass('disabled');
      $('#chemical-tab').addClass('disabled');
    }
    if (key == 'C') {
      this.chemDisplay = false;
      $('#analysis-tab').addClass('disabled');
      $('#water-tab').addClass('disabled');
    }
    console.log(item.rangeMin)
    console.log(item.rangeMax)    

    this.itemForm.setValue({
      itemName: item.itemName,
      range: {
        sign: item.operator,
        minRange: item.rangeMin,
        maxRange: item.rangeMax,
      },
      chemicalName: item.chemicalName
    });
  }

  saveItem(key: any) {
    this.loading = true;
    this.rItems = [];

    if (this.editMode) {

      if (key == 'A') {             
        this.consumService.editReadingItem(this.itemForm.value, this.itemId, this.aId).subscribe(data => {          
          this.loadingPage = false;
          this.activeKey = 'A';
          this.analysisDisplay = true;
          //keep load function here to skip the service wait time with spinner bootstrap          
          this.getTypes();
        })
      }
      if (key == 'W') {
        this.consumService.editReadingItem(this.itemForm.value, this.itemId, this.wId).subscribe(data => {
          this.loadingPage = false;
          this.activeKey = 'W';
          this.waterDisplay = true;
          //keep load function here to skip the service wait time with spinner bootstrap                    
          this.getTypes();
        })
      }
      if (key == 'C') {
        this.consumService.editReadingItem(this.itemForm.value, this.itemId, this.cId).subscribe(data => {
          this.loadingPage = false;
          this.activeKey = 'C';
          this.chemDisplay = true;
          //keep load function here to skip the service wait time with spinner bootstrap          
          this.getTypes();
        })
      }

    }
    else {
      if (key == 'A') {
        this.consumService.addReadingItem(this.itemForm.value, this.aId).subscribe(data => {
          this.loadingPage = false;
          this.activeKey = 'A';
          this.analysisDisplay = true;
          //keep load function here to skip the service wait time with spinner bootstrap          
          this.getTypes();
        })
      };
      if (key == 'W') {
        this.consumService.addReadingItem(this.itemForm.value, this.wId).subscribe(data => {
          this.loadingPage = false;
          this.activeKey = 'W';
          this.waterDisplay = true;
          //keep load function here to skip the service wait time with spinner bootstrap          
          this.getTypes();
        })
      };
      if (key == 'C') {
        this.consumService.addReadingItem(this.itemForm.value, this.cId).subscribe(data => {
          this.loadingPage = false;
          this.activeKey = 'C';
          this.chemDisplay = true;
          //keep load function here to skip the service wait time with spinner bootstrap          
          this.getTypes();
        })
      };
    }

  }

  deleteItemConfirm(item: any) {
    this.itemId = item.id;

    this.deleteMessage = '';
  }

  deleteItem() {
    this.deleteMessage = "Item is being removed..";
    this.delete = true;

    this.consumService.deleteReadingItem(this.itemId).subscribe(data => {
      if (data == null) {
        $('#staticBackdrop').modal('hide');
        this.rItems = this.rItems.filter((x: any) => x.id !== this.itemId)
        this.analysis = this.analysis.filter((x: any) => x.id !== this.itemId)
        this.water = this.water.filter((x: any) => x.id !== this.itemId)
        this.chemical = this.chemical.filter((x: any) => x.id !== this.itemId)

        this.delete = false;
      }
    });
  }

  cancelItem(key: any) {
    if (key == 'A') {
      this.analysisDisplay = true;
      $('#water-tab').removeClass('disabled');
      $('#chemical-tab').removeClass('disabled');
    }
    if (key == 'W') {
      this.waterDisplay = true;
      $('#analysis-tab').removeClass('disabled');
      $('#chemical-tab').removeClass('disabled');
    }
    if (key == 'C') {
      this.chemDisplay = true;
      $('#water-tab').removeClass('disabled');
      $('#analysis-tab').removeClass('disabled');
    }
  }

}
