import { Component, OnInit } from '@angular/core';
import {Chart} from 'node_modules/chart.js';
@Component({
  selector: 'app-chart1',
  templateUrl: './charts1.component.html',
  styleUrls: ['./charts1.component.css']
})
export class Charts1Component implements OnInit {
  
  constructor() { }

  ngOnInit(): void {
    
    var custName='Vacuum';
    var Litres=12;
    const xLabels_1=[custName, 'Plant', 'Mixer', 'Lab',custName, 'Plant', 'Mixer', 'Lab',custName, 'Plant', 'Mixer', 'Lab',custName, 'Plant', 'Mixer', 'Lab',custName, 'Plant', 'Mixer', 'Lab',custName, 'Plant', 'Mixer', 'Lab',custName, 'Plant', 'Mixer', 'Lab',custName, 'Plant', 'Mixer', 'Lab',custName, 'Plant', 'Mixer', 'Lab',custName, 'Plant', 'Mixer', 'Lab',custName, 'Plant', 'Mixer', 'Lab',custName, 'Plant', 'Mixer', 'Lab','Date1','Date2'];
    
    const Data1=[762, 503, 708, 280,406, 800, 760,1080,802,1500, 1000,509,762, 503, 708, 280,406, 800, 760,500,802,350, 1000,509,762, 503, 708, 680,406, 800, 760,1080,802,1020, 1000,509,762, 503, 708, 480,406, 800, 760,780,802,600,800,509,708,500] ;   
    const Data2=[Litres, 30, 30, 10,Litres, 30, 30, 10,Litres, 30, 30, 10,Litres, 30, 30, 10,Litres, 30, 30, 10,Litres, 30, 30, 10];
    
    const Data3=[20, 315, 30, 10,Litres, 30, 45, 60,Litres, 70, 30, 80,Litres, 30, 30, 10,140, 30, 30, 90,Litres,153, 230, 170];
    
    const Data4=[200, 415, 300, 100,Litres, 230, 345, 760,Litres, 70, 30, 80,Litres, 30, 30, 10,140, 30, 30, 90,Litres,153, 230, 170];
    
    const Data5=[300, 405, 350, 170,973, 270, 375, 760,Litres, 70, 300, 80,Litres, 30, 30, 10,140, 30, 30, 90,Litres,153, 230, 170];
    
    const Data6=[200, 415, 300, 100,Litres, 230, 345, 760,Litres, 70, 30, 80,Litres, 30, 30, 10,140, 30, 30, 90,Litres,153, 230, 170];
    
    var myChart = new Chart("chart-2", {
      type: 'line',
      data: {
         labels: xLabels_1,
             datasets: [{
              label: 'PH Levels',
              data:Data1,
              fill:false,
              //display:true,
              backgroundColor: [
                  'white'
                //'rgba(54, 203, 235, 1)',
                  //'rgba(54, 162, 235, 0.2)',
                  //'rgba(255, 206, 86, 0.2)',
                  //'rgba(75, 192, 192, 0.2)',
                //'rgba(153, 102, 255, 0.2)',
                 // 'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'blue'
                 /* 'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'*/
              ],
              borderWidth: 3
          },
        
          {
            label: 'ORP',
            data: Data2, 
            fill:false,
            backgroundColor: [
                'green',
               ],
            borderColor: [
                'green'
              
            ],
            borderWidth: 3
        },
        {
            label: 'Iron',
            data: Data3, 
            fill:false,
            backgroundColor: [
                'red',
               
            ],
            borderColor: [
                'red'
              
            ],
            borderWidth: 3
        },
        {
            label: 'Conductivity',
            data: Data4, 
            fill:false,
            backgroundColor: [
                'red',
               
            ],
            borderColor: [
                'brown'
              
            ],
            borderWidth: 3
        },
        {
            label: 'Temperature',
            data: Data5, 
            fill:false,
            backgroundColor: [
                'red',
               
            ],
            borderColor: [
                'orange'
              
            ],
            borderWidth: 3
        },
        {
            label: 'FAH',
            data: Data4, 
            fill:false,
            backgroundColor: [
                'red',
               
            ],
            borderColor: [
                'red'
              
            ],
            borderWidth: 3
        },
        {
            label: 'Chlorides',
            data: Data4, 
            fill:false,
            backgroundColor: [
                'red',
               
            ],
            borderColor: [
                'red'
              
            ],
            borderWidth: 3
        },
        {
            label: 'Inhibitor',
            data: Data4, 
            fill:false,
            backgroundColor: [
                'red',
               
            ],
            borderColor: [
                'red'
              
            ],
            borderWidth: 3
        },
    
    
        {
            label: 'M Alkalinity',
            data: Data4, 
            fill:false,
            backgroundColor: [
                'red',
               
            ],
            borderColor: [
                'red'
              
            ],
            borderWidth: 3
        },{
            label: 'HCC & Legionella',
            data: Data4, 
            fill:false,
            backgroundColor: [
                'green',
               
            ],
            borderColor: [
                'red'
              
            ],
            borderWidth: 3
        },
        {
            label: 'W.Meter kL',
            data: Data4, 
            fill:false,
            backgroundColor: [
                'green',
               
            ],
            borderColor: [
                'yellow'
              
            ],
            borderWidth: 3
        },
        {
            label: 'Water Consumption(kL/Day) ',
            data: Data4, 
            fill:false,
            backgroundColor: [
                'green',
               
            ],
            borderColor: [
                'yellow'
              
            ],
            borderWidth: 3
        },{
            label: 'Avg. Range(kL/Day)',
            data: Data6, 
            fill:false,
            backgroundColor: [
                'green',
               
            ],
            borderColor: [
                'voilet'
              
            ],
            borderWidth: 3
        }],
          
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true

                  }
              }]
          }
      }
  });
  function addData1(chart, label, data) {
    //chart.data.labels.push(label);
  /*  chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });*/
    xLabels_1.push(label)
    Data1.push(data);
    chart.update();
}
addData2(myChart,'newLabel',700);
function addData2(chart, label, data) {
   // chart.data.labels.push(label);
  /*  chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });*/
    
    Data2.push(data);
    chart.update();
}

function addData3(chart, label, data1,data2,data3) {
    // chart.data.labels.push(label);
   /*  chart.data.datasets.forEach((dataset) => {
         dataset.data.push(data);
     });*/
     
     Data3.push(data1);
     Data4.push(data2);
     Data5.push(data3);
     chart.update();
 }
addData2(myChart,'newLabel',70);
addData1(myChart,'newLabel',700);
for(var i=1;i<20;i++)
{
     var v=i;
     var v1=i*3;
     var v2=i*5;
    if(v%2==0){
        addData3(myChart,'val2',v,v1,v2);  
    }

    else
    {
        addData3(myChart,'val2',v,10*v1,20*v2);
    }

   // addData3(myChart,'val2',175);
        
}

function removeData(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    chart.update();
}




















//removeData(myChart);
  var abc="Customer";
  
  }

}
