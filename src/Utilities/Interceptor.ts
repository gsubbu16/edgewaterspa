import { Injectable, Injector } from '@angular/core';
//import { LocalStorageUtils } from './local-storage-utils';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Router, ParamMap } from '@angular/router';

@Injectable()
export class Interceptor implements HttpInterceptor {


    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        console.log('Intercepted HTTP call', request);
        return next.handle(request);

    }




}